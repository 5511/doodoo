const base = require("./../base");
const moment = require("moment");

module.exports = class extends base {
    async _initialize() {
        await super.isWxaAuth();
        await super.isShopAuth();
        await super.isUserAuth();
    }

    /**
     *
     * @api {get} /shop/api/shop/coupon/index 可用优惠券
     * @apiDescription 可用优惠券
     * @apiGroup Shop/api
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} Referer 小程序referer.
     *
     * @apiParam {Number} totalprice   订单总价
     *
     * @apiSampleRequest /shop/api/shop/coupon/index
     *
     */
    async index() {
        const { totalprice } = this.query;
        const shopId = this.state.shop.id;
        const user = this.state.user.id;

        const coupon = await this.model("user_coupon")
            .query(qb => {
                qb.join(
                    "shop_coupon",
                    "shop_coupon.id",
                    "shop_user_coupon.coupon_id"
                );
                qb.where("shop_user_coupon.status", 0);
                qb.where("shop_user_coupon.shop_id", shopId);
                qb.where("shop_user_coupon.user_id", user);
                qb.where("shop_coupon.limit_price", "<=", totalprice);
                qb.where(
                    "shop_coupon.started_at",
                    "<=",
                    moment().format("YYYY-MM-DD HH:mm:ss")
                );
                qb.where(
                    "shop_coupon.ended_at",
                    ">=",
                    moment().format("YYYY-MM-DD HH:mm:ss")
                );
                qb.where("shop_coupon.status", 1);
            })
            .fetchAll({ withRelated: ["coupon"] });
        this.success(coupon);
    }

    /**
     *
     * @api {get} /shop/api/shop/coupon/grtCoupon 领取优惠券
     * @apiDescription 领取优惠券
     * @apiGroup Shop/api
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} Referer 小程序referer.
     *
     * @apiParam {Number} id 优惠券ID
     *
     * @apiSampleRequest /shop/api/shop/coupon/grtCoupon
     *
     */

    async grtCoupon() {
        const { id } = this.query;

        const shop_id = this.state.shop.id;
        const user_id = this.state.user.id;

        const menu = await this.model("coupon")
            .query(qb => {
                qb.where("id", "=", id);
                qb.where("status", "=", 1);
            })
            .fetch();
        if (!menu) {
            this.fail("优惠劵已领完或活动已结束");
            return;
        }
        let coupon = "";
        const res = await this.model("user_coupon")
            .query(qb => {
                qb.where("coupon_id", "=", id);
                qb.where("shop_id", "=", shop_id);
            })
            .fetchAll();
        if (Number(menu.num) > 0 && Number(menu.has_num) >= Number(menu.num)) {
            this.fail("已领完");
            return;
        }

        if (res.length) {
            if (
                Number(menu.limit_num) > 0 &&
                Number(res.length) >= Number(menu.limit_num)
            ) {
                this.fail("已领过");
                return;
            }
        }
        const obj = {
            shop_id: shop_id,
            coupon_id: id,
            user_id: user_id
        };
        coupon = await this.model("user_coupon")
            .forge(obj)
            .save();

        this.success(coupon);
    }

    /**
     *
     * @api {get} /shop/api/shop/coupon/userCoupon 用户优惠券
     * @apiDescription 用户优惠券
     * @apiGroup Shop/api
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} Referer 小程序referer.
     *
     * @apiParam {Number} status 状态
     *
     * @apiSampleRequest /shop/api/shop/coupon/userCoupon
     *
     */
    async userCoupon() {
        const shopId = this.state.shop.id;
        const user = this.state.user.id;
        const { status = 0 } = this.query;
        const coupon = await this.model("user_coupon")
            .query(qb => {
                qb.join(
                    "shop_coupon",
                    "shop_coupon.id",
                    "shop_user_coupon.coupon_id"
                );
                qb.where("shop_user_coupon.user_id", user);
                qb.where("shop_user_coupon.shop_id", shopId);
                if (Number(status) !== -1) {
                    console.log(status);
                    qb.where("shop_user_coupon.status", status);
                }
                if (Number(status) === 0) {
                    console.log(111);
                    // qb.where("shop_user_coupon.status", status);
                    qb.where(
                        "shop_coupon.ended_at",
                        ">=",
                        moment().format("YYYY-MM-DD HH:mm:ss")
                    );
                }
                if (Number(status) === -1) {
                    console.log(2222);
                    qb.where(
                        "shop_coupon.ended_at",
                        "<=",
                        moment().format("YYYY-MM-DD HH:mm:ss")
                    );
                }
                qb.where("shop_coupon.status", 1);
            })
            .fetchAll({ withRelated: ["coupon"] });
        this.success(coupon);
    }

    /**
     *
     * @api {get} /shop/api/shop/coupon/coupon 优惠券列表
     * @apiDescription 优惠券列表
     * @apiGroup Shop/api
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} Referer 小程序referer.
     *
     *
     * @apiSampleRequest /shop/api/shop/coupon/coupon
     *
     */
    async coupon() {
        const shopId = this.state.shop.id;
        await this.model("coupon")
            .query(qb => {
                qb.where("shop_id", shopId);
                qb.where("status", 1);
                qb.where(
                    "ended_at",
                    "<=",
                    moment().format("YYYY-MM-DD HH:mm:ss")
                );
                qb.update({ status: 0 });
            })
            .fetchAll();
        const coupon = await this.model("coupon")
            .query(qb => {
                qb.where("shop_id", shopId);
                qb.where("status", 1);
            })
            .fetchAll();
        this.success(coupon);
    }
};
