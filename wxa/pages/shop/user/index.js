//获取应用实例
const app = getApp();
const util = require("../../../utils/util.js");

Page({
    data: {
        Loading: 0,
        user_info: {},
        color: wx.getExtConfigSync().color,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
    },
    onShow: function() {
        app.getUserInfo(user_info => {
            this.setData({
                userInfo: user_info
            });
        });
    },
    lookOverAll() {
        wx.navigateTo({
            url: '/pages/shop/order/order-list/index'
        })
    },
    goOrder(e) {
        wx.navigateTo({
            url: `/pages/shop/order/order-list/index?type=${e.currentTarget.dataset.type}`
        })
    },
    goApp(e) {
        // type  0：优惠券 1：收藏 2：收货地址
        const type = e.currentTarget.dataset.type;
        if (type==0) {
            wx.navigateTo({
                url: '/pages/shop/coupons/index'
            })
        }
        if (type==1) {
            wx.navigateTo({
                url: '/pages/shop/collection/index'
            })
        }
        if (type==2) {
            wx.chooseAddress({
                success: function(res) {
                    console.log(res);
                    console.log(res.userName);
                    console.log(res.postalCode);
                    console.log(res.provinceName);
                    console.log(res.cityName);
                    console.log(res.countyName);
                    console.log(res.detailInfo);
                    console.log(res.nationalCode);
                    console.log(res.telNumber);
                }
            });
        }

        if(type==3){
          wx.navigateTo({
            url: '/pages/fx/index'
          })
        }
    },
    //下拉刷新
    onPullDownRefresh: function() {
        setTimeout(() => {
            wx.stopPullDownRefresh();
        }, 600)
    }
});